<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags_on_Post extends Model
{
    protected $fillable = [
		'post_id', 'tag_id'
	];
    
    public function tag() {
        return $this->belongsTo(App\Tag);
    }
    
    public function post() {
        return $this->belongsTo(App\Post);
    }
}
