<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
		'title', 'user_id', 'description', 'img', 'category_id'
	];
    
    public function comment() {
        return $this->hasMany('App\Comment', 'comment_id','id');
    }
    
    public function tags_on_post() {
        return $this->hasMany(App\Tags_on_Post);
    }
    
    public function ticket() {
        return $this->hasMany(App\Ticket);
    }
    
    public function favorite() {
        return $this->hasMany(App\Favorite);
    }
    
    public function user() {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }
    
    public function category() {
        return $this->belongsTo('App\Category', 'category_id', 'id');
    }
}
 