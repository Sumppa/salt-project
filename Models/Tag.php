<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
		'title'
	];
    
    public function tags_on_post() {
        return $this->hasMany(App\Tags_on_Post);
    }
}
