<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    protected $fillable = [
		'reason', 'description', 'post_id', 'user_id'
	];   
    
    public function user() {
        return $this->belongsTo(App\User);
    }
    
    public function post() {
        return $this->belongsTo(App\Post);
    }
}
