<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $fillable = [
		'user_id', 'following_id'
	];
    
    public function user() {
        return $this->belongTo(App\User);
    }
    
    public function post() {
        return $this->belongTo(App\Post);
    }
}
