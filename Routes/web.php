<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect ('index');
});
// Routes for editing user information
Route::get('profile/edit', 'EditUserController@edit');
Route::post('profile/edit', 'EditUserController@update');

Route::get('report', 'ViewController@report');

// Route for searching images and users
Route::post('search','SearchController@search');

// Routes for editing post
Route::get('posts/edit/{id}', 'PostsController@edit');
Route::post('posts/edit', 'PostsController@update_edit');

// Route for liking post
Route::get('posts/like/{id}', 'CommentsController@like');

// Route for commenting
Route::post('posts/comments/{id}', 'CommentsController@saveComment');

// Redirect to index
Route::get('index', function () {
    return redirect('index/newest');
});

// Route for sorting index page images
Route::get('index/{sort_act}', 'IndexController@index');

// Route for deleting post
Route::delete('posts/{id}', 'PostsController@delete');

// Route for deleting comment
Route::post('posts/{id}', 'PostsController@deleteComment');

// Route for deleting user
Route::post('profile/{id}', 'UserController@delete_profile');

// Route for creating new post
Route::get('upload', 'PostsController@create');

// Route for showing single post
Route::get('posts/{id}', 'PostsController@show');

// Route for creating new post
Route::post('posts', 'PostsController@store');

// Logout
Route::get('/logout', function() {
	Auth::logout();
	return redirect('index');
});

//Route::get('posts', 'PostsController@index');

// Route for user page
Route::get('profile/{id}', 'UserController@show');

Route::auth();

