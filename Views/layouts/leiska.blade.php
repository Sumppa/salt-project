<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Salt</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/thumbnail-gallery.css" rel="stylesheet">
      
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Comfortaa" rel="stylesheet">

  </head>

  <body>
	
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg1 fixed-top" id="bar">
      <div class="container">
        <a class="nav-link" href="http://salt.labranet.jamk.fi/index/newest"><img class="mw-100 w-xs-50 mauto logo" src="/storage/ehmlog.png" alt=""></a>
          <div id="bufferwide"></div>
        
          
        <!-- Search bar -->
        <!--  <div class="ml-3 col-lg-6">
            <div class="input-group">
                <form method="post" action="SearchController@search">
                    <span class="input-group-btn">
                    <button class="btn btn-dark border border-right-0" type="button" >Go!</button>
                    </span>
                    <input name="search" type="text" class="form-control input-group-addon" placeholder="Search for..." >
                </form>
            </div>
          </div>-->
        <div class="form-row">
            <a class="nav-link col-2" href="http://salt.labranet.jamk.fi/index/newest"><img class="mw-100 w-xs-50 mauto logosmall" src="/storage/ehmlog.png" alt=""></a>
            <div class="col">
            {!! Form::open(['action'=>'SearchController@search', 'method' => 'POST']) !!}

            {!! Form::text('search', '', ['class' => 'form-control input-group-addon', 'placeholder' => 'Search for...']) !!}
            </div>
            <div class="col">
            {!! Form::submit('Go', ['class' => 'btn border border-right-0']) !!}

            {!! Form::close() !!}
            </div>
            <button class="navbar-toggler col-2" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        </div>
          
        <!-- Nav buttons -->
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            
            <li class="nav-item">
              <a class="nav-link" id="doot" href="{{ url('index') }}">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            @if (Auth::guest())
                <li><a class="nav-link" id="doot" href="{{ route('login') }}">Login</a></li>
                <li><a class="nav-link" id="doot" href="{{ route('register') }}">Register</a></li>
                
            @else
                <li><a class="nav-link" id="doot" href="{{ url('upload') }}">Upload</a></li>
                <li><a class="nav-link" id="doot" href="{{ route('logout') }}">Log Out</a></li>
                <li><a class="" href="http://salt.labranet.jamk.fi/profile/{{Auth::user()->id}}"><img id="nav-img" src="/storage/avatars/{{Auth::user()->img}}"></a></li>
            @endif
          </ul>
        </div>
        
      </div>
    </nav>
      
    @yield('content')
      
    <div id="buffer"></div>
      
    <!-- Footer -->
    <footer class="footer pt-2 mt-4">
      <div class="">
        <a class="flink" href="{{ url('report') }}">Raportti</a>
	 </div> <!-- /.container -->
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="/js/jquery.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>

  </body>

</html>