@extends('layouts.leiska')
@section('content')
<div class="row border-dark nib">
        <div class="my-3 ml-3 col-lg-3 col-md-3 col-sm-3 col-xs-1">
        </div>
        <div class="profiiltext text-left ml-3 mt-3 col-8">
</div>
    <div class="container">
        {!! Form::open(['action'=>'PostsController@update_edit', 'method' => 'POST']) !!}

          {!! Form::hidden('id', $post->id ) !!}
		  {!! Form::label('title', 'Title:') !!}
		  {!! Form::text('title', $post->title, ['class' => 'form-control']) !!}
		  <br/>
		  {!! Form::label('description', 'Description:') !!}
		  {!! Form::textarea('description', $post->description, ['class' => 'form-control']) !!}
		  <br/>
		  {!! Form::submit('Confirm edit', ['class' => 'btn float-left', 'id' => 'nappi']) !!}

	   {!! Form::close() !!}
            
</div>
@endsection