@extends('layouts.leiska')
@section('content')
    <div class="" id="picbg">
        <div class="container">
            <img class="img-fluid" id="img-big" src="/storage/{{ $posts->user->name }}/{{ $posts->img }}" alt="asdassdaspodkaspodksad">
        </div>
    </div>
    <div id="main" class="container">
        <article>
            <div id="postarea" class="img-thumbnail">
                <div>
                    
                    <!--delete post-->
                    @if (Auth::guest())
                    @elseif ($posts->user_id == Auth::user()->id)
                        
                        {!! Form::open(['action'=>['PostsController@delete', $posti], 'method' => 'POST', 'onsubmit' => 'return ConfirmDelete()']) !!}
                        {!! Form::hidden('_method', 'DELETE')!!}
                        {!! Form::submit('Delete', ['class' => 'btn', 'id' => 'nappi']) !!}
                        {!! Form::close() !!}
                    @endif  
                    
                    @if (Auth::guest())
                        @elseif ($posts->user_id == Auth::user()->id)
                            <a class="pedit btn" id="nappi" href="http://salt.labranet.jamk.fi/posts/edit/{{ $posts->id }}">edit</a>
                        @endif
                    <!--admin delete
                    @if (Auth::guest())
                    @elseif ($posts->user_id == Auth::user()->id)
                    @elseif (Auth::user()->admin == 1)
                        {!! Form::open(['action'=>['PostsController@delete', $posti], 'method' => 'POST']) !!}
                        {!! Form::hidden('_method', 'DELETE')!!}
                        {!! Form::submit('Delete', ['class' => 'btn', 'id' => 'nappi']) !!}
                        {!! Form::close() !!}
                    @endif  -->
                    
                </div>
                <div>
                    <div class="col row" id="profid">
                        <a href="http://salt.labranet.jamk.fi/profile/{{ $posts->user_id }}"><img class="profilepicsmall" src="/storage/avatars/{{ $posts->user->img }}"></a>
                        <a id="doot2" class="puser" href="http://salt.labranet.jamk.fi/profile/{{ $posts->user_id }}">{{ $posts->user->name }}</a>
                    </div>
                    <div class="col">
                            <div class="row" id="score">
                                <h3 class="number">{{ $count }}</h3>
                                @if (Auth::guest())
                                    <a class="nav-link" href="http://salt.labranet.jamk.fi/login"><img class="icon" src="/storage/heart.png" alt=""></a>
                                @else
                                    <a class="nav-link" href="http://salt.labranet.jamk.fi/posts/like/{{ $posti->id }}"><img class="icon" src="/storage/heart.png" alt=""></a>
                                @endif
                            </div>
                        <hr>
                            <h2 id="ptitle">{{ $posts->title }}</h2>
                        
                        <p id="pdesc">{{ $posts->description }}</p>
                        <p class="timestamp">{{ $posts->category->title }} {{ $posts->published_at }}</p>
                    </div>
                </div>
            </div>
            
            <div id="comment">
                
                
                
                <!--add comment-->
                @if (Auth::guest()) 
                @else
                {!! Form::open(['action'=>['CommentsController@saveComment', $posti], 'method' => 'POST']) !!}
                        {!! Form::label('commenttext', 'Comment:') !!}
                        {!! Form::textarea('commenttext', null, ['class' => 'form-control descrootie']) !!}
                        {!! Form::hidden('post_id', $posti->id)!!}
                        {!! Form::submit('Add Comment', ['class' => 'btn float-left', 'id' => 'nappi' ]) !!}
                    {!! Form::close() !!}
                @endif
                
                <!--like-->
            </div>
            <div id="comments">
                @foreach ($comments as $comment)
                <!--delete comment-->
                    <div class="img-thumbnail comment row">            
                        <div class="col-1.5"><a href="/profile/{{ $comment->user_id }}"><img class="mw-100 w-xs-50 mauto" id="com-img" src="/storage/avatars/{{ $comment->user->img }}" alt=""></a></div>
                        <div class="col">
                            <a class="linkid" id="doot2" href="/profile/{{ $comment->user_id }}">{{ $comment->user->name }}</a>
                            <p class="">{{ $comment->commenttext }}</p>
                            <p class="timestamp">{{ $comment->published_at }}</p>
                        </div>
                        @if (Auth::guest())
                    @elseif ($comment->user_id == Auth::user()->id)
                        {!! Form::open(['action'=>['PostsController@deleteComment', $comment], 'method' => 'POST', 'onsubmit' => 'return ConfirmDeleteComment()']) !!}
                        {!! Form::submit('Delete', ['class' => 'btn', 'id' => 'nappi']) !!}
                        {!! Form::close() !!}
                    @endif 
                    </div>
                @endforeach
            </div>
        </article>
    </div>
    <script>

      function ConfirmDelete()
      {
      var x = confirm("Are you sure you want to delete this post?");
      if (x)
        return true;
      else
        return false;
      }
        
        function ConfirmDeleteComment()
      {
      var x = confirm("Are you sure you want to delete this comment?");
      if (x)
        return true;
      else
        return false;
      }

    </script>
@endsection