@extends('layouts.leiska')
@section('content')
    <div class="container">
        <h2 id="uploadhead">Creating post</h2>
        <hr/>
        {!! Form::open(['action'=>'PostsController@store', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

            {!! Form::label('title', 'Title:') !!}
            {!! Form::text('title', null, ['class' => 'form-control']) !!}
            <br/>
            {!! Form::label('description', 'Description:') !!}
            {!! Form::textarea('description', null, ['class' => 'form-control descrootie']) !!}
            <br/>
            {!! Form::hidden('user_id', $id) !!}
            <br/>
            {!! Form::label('img', 'Img:') !!}<br/>
            {!! Form::file('img', null, ['class' => 'form-control']) !!}
            <br/>
            <br/>
            {!! Form::label('category_id', 'Category:') !!}
            {!! Form::select('category_id', array('2' => 'Painting', '3' => 'Photo', '5' => 'Digital', '6' => 'Drawing')) !!}

            <br/>
            {!! Form::submit('Add Post', ['class' => 'btn float-left', 'id' => 'nappi']) !!}

        {!! Form::close() !!}
    </div>
@endsection