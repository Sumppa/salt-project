@extends('layouts.leiska')
@section('content')
<div class="row border-dark nib">
    <div class="my-3 ml-3 col-lg-3 col-md-3 col-sm-3 col-xs-1"></div>
    <div class="profiiltext text-left ml-3 mt-3 col-8">
        <h2>{{{ isset(Auth::user()->name) ? Auth::user()->name : Auth::user()->name }}}</h2>
        <p>{{{Auth::user()->description }}}</p>
    </div>
    <div class="container">
        <!--
        <form method="POST" action="EditUserController@update" enctype="multipart/form-data">
            <div class="form-group">
            {{ csrf_field() }}
            {{ method_field('patch') }} 
                
            <h2>Email</h2>
            <input type="text" name="email"  value="{{ $user->email }}" />
        
            <h2>Password</h2>
            <input type="password" name="password"/>
            
            <h2>Description</h2>
            <textarea name="description" value="description">{{$user->description}}</textarea><br>
                
            <h2>Profile picture</h2>
            <input name="img" type="file">
            <br><br>
            
            <button type="submit">Confirm edit</button>
            </div>
        </form> -->
        {!! Form::open(['action'=>'EditUserController@update', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

          {!! Form::hidden('id', $user->id ) !!}
		  {!! Form::label('email', 'Email:') !!}
		  {!! Form::email('email', $user->email, ['class' => 'form-control']) !!}
		  <br/>
		  {!! Form::label('description', 'Description:') !!}
		  {!! Form::textarea('description', $user->description, ['class' => 'form-control']) !!}
		  <br/>
          {!! Form::label('img', 'Profile picture:') !!}
          {!! Form::file('img', null, ['class' => 'form-control']) !!}
		  <br/>
            <br/>
            {!! Form::label('password', 'Password:') !!}
		  {!! Form::password('password', null, ['class' => 'form-control']) !!}
		  <br/>
		  {!! Form::submit('Confirm edit', ['class' => 'btn float-left', 'id' => 'nappi']) !!}

	   {!! Form::close() !!}
            
    </div>
</div>
@endsection