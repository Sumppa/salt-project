@extends('layouts.leiska')
@section('content')
    <div class="border-dark nib buffertop">
        <div class="container" id="profiledark">
            
                @if (Auth::guest())

                @elseif ($id == Auth::user()->id)
                    <a class="btn" id="nappi1" href="{{ url('profile/edit') }}">Edit</a>
                @endif
                @if (Auth::guest())
                    @elseif ($users->id == Auth::user()->id)
                        {!! Form::open(['action'=>['UserController@delete_profile', $users], 'method' => 'POST', 'onsubmit' => 'return ConfirmDelete()']) !!}
                        
                        {!! Form::submit('Delete', ['class' => 'btn', 'id' => 'nappi1']) !!}
                        {!! Form::close() !!}
                @endif  
            <div class="row">
                <div class="my-3 ml-3 col-lg-3 col-md-3 col-sm-3 col-xs-1">
                    <a>  
                        <img class="profilepic" src="/storage/avatars/{{ $users->img }}" alt="{{{$users->name }}}" title="{{{$users->name }}}">
                    </a>
                </div>
                <div class="profiiltext text-left ml-3 mt-3 col-8">
                    <h2 class="profname">{{{$users->name }}}</h2>
                    <div id="line"></div>
                    <p>{{{$users->description }}}</p>
                    <!--delete post-->
                </div>
            </div>
        </div>
    </div>
    <!-- Page Content -->
    <div class="container">
        
      <h1 class="my-4 text-center text-lg-left">Gallery</h1>

      <div class="row text-center text-lg-left">
        @foreach ($posts as $post)
            <div class="col-lg-3 col-md-4 col-xs-6">
                <div class="cont hovereffect side-crop">
                        <img class="img-fluid img-responsive image" id="kuvat" src="/storage/{{ $post->user->name }}/{{ $post->img }}" alt="{{ $post->user->name }}">
                        
                        <div class="overlay">
                            <a class="nav-link post-link col text-white text1" href="/posts/{{ $post->id }}">{{ $post->title }}</a>
                            <a class="nav-link post-link col text-white text2" href="/profile/{{ $post->user_id }}">{{ $post->user->name }}</a>
                        </div>
                    </div>
            </div>
            <div id="mobilelinks" class="row">
                <a class="nav-link post-link col text2" href="/posts/{{ $post->id }}">{{ $post->title }}</a>
            </div>
        @endforeach
        </div>


    </div>
    <!-- /.container -->
    <script>

      function ConfirmDelete()
      {
      var x = confirm("Are you sure you want to delete your profile?");
      if (x)
        return true;
      else
        return false;
      }

    </script>
@endsection

