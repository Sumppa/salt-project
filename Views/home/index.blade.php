@extends('layouts.leiska')
@section('content')
    <div class="container">

        <h1 class="my-4 text-center text-lg-left">Home</h1>

            <!--sort-->
            <ul class="row text-center text-lg-left">
                <li class="nav-link"><a id="sort"  href="/index/newest">Newest</a></li>
                <li class="nav-link"><a id="sort"  href="/index/oldest">Oldest</a></li>
                
            </ul>
            
        <div class="row">
        @foreach ($posts as $post)
            <div class="col-lg-3 col-md-5 col-xs-6 picscontainer">
                    <div class="cont hovereffect side-crop">
                        <img class="img-fluid img-responsive image" id="kuvat" src="/storage/{{ $post->user->name }}/{{ $post->img }}" alt="{{ $post->user->name }}">
                        
                        <div class="overlay">
                            <a class="nav-link post-link col text-white text1" href="/posts/{{ $post->id }}">{{ $post->title }}</a>
                            <a class="nav-link post-link col text-white text2" href="/profile/{{ $post->user_id }}">{{ $post->user->name }}</a>
                        </div>
                    </div>
            </div>
            <div id="mobilelinks" class="row">
                <a class="nav-link post-link col text2" href="/posts/{{ $post->id }}">{{ $post->title }}</a>
            </div>
        @endforeach
        </div>
        <div class="container" id="paginate">
            {{ $posts->links() }}
        </div>

    </div> <!-- /.container -->
@endsection