@extends('layouts.leiska')
@section('content')

    <!-- Page Content -->
    <div class="container">
        
      <h1 class="my-4 text-center text-lg-left">Search results</h1>

     <h2>Posts</h2>
      <div class="row text-center text-lg-left">
            

            @if (count($posts) > 0)
                @foreach ($posts as $post)
                    <div class="col-lg-3 col-md-4 col-xs-6">
                    <div class="cont hovereffect side-crop">
                        <img class="img-fluid img-responsive image" id="kuvat" src="/storage/{{ $post->user->name }}/{{ $post->img }}" alt="{{ $post->user->name }}">
                        
                        <div class="overlay">
                            <a class="nav-link post-link col text-white text1" href="/posts/{{ $post->id }}">{{ $post->title }}</a>
                            <a class="nav-link post-link col text-white text2" href="/profile/{{ $post->user_id }}">{{ $post->user->name }}</a>
                        </div>
                    </div>
            </div>
            <div id="mobilelinks" class="row">
                <a class="nav-link post-link col text2" href="/posts/{{ $post->id }}">{{ $post->title }}</a>
            </div>
            @endforeach

            @else
                <div class="nopost"><p>No results!</p></div>
            @endif
        </div>
        <div id=split></div>
        <h2>Users</h2>
        <div class="row text-center text-lg-left">
              
            @if (count($users) > 0)
            @foreach ($users as $user)
                <div class="col-lg-2 col-md-3 col-xs-6">
                    <div class="cont hovereffect side-crop2">
                        <!--<a href="/profile/{{$user->id}}"><p class="overlay">{{$user->name}}</p></a>-->
                        <img id="kuvat" class="img-fluid img-responsive image" src="/storage/avatars/{{ $user->img }}">
                        <div class="overlay">
                            <a class="nav-link post-link col text-white text1" href="/profile/{{$user->id}}">{{$user->name}}</a>
                        </div>
                    </div>
                </div>
            @endforeach

            @else
                <div class="nopost"><p>No results!</p></div>
            @endif
        </div>
    </div>
    <!-- /.container -->
@endsection

