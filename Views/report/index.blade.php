@extends('layouts.leiska')
@section('content')
    <!-- Page Content -->
    <div class="container rapsa">
        
        <!--BOLDATUT EDIT TAI MÄKEEN-->
        
      <img id="teamlogo" src="/storage/teamlogo.png" alt="logo">
      <h1 class="my-4 text-center text-lg-left">Loppuraportti</h1>
        <div class="rapsa-article">
            <h2>Kuvapalvelu - Salt</h2>
            <p>Web-Projekti 2</p>
            <p>25.4.2018</p>
            <p>
                Ryhmän jäsenet:
                Sasu Karvonen K8672,
                Laura Pekkanen K8966,
                Taneli Sormunen K8920 ja
                Aarni Ylhäinen K8406.
            </p>
        </div>
        
        <div class="rapsa-article">
        <h2>Kuvaus</h2>
            <p>Sovellus on syksyllä Web-palvelujen määrittely ja suunnittelu -opintojaksolla suunniteltu kuvapalvelu. Sovelluksessa kirjautuneet käyttäjät voivat luoda postauksia, tykätä postauksista ja kommentoida niitä. Käyttäjä pystyy poistamaan omia kuviaan, kommenttejaan ja myös oman profiilinsa. Postaukselle on annettava otsikko, kuva ja kategoria, kuvaus on vapaaehtoinen. Näitä kenttiä pystyy myös myöhemmin muokkamaan. </p>

            <p>Kirjautumattomat käyttäjät pystyvät selaamaan kuvia etusivulla, avaamaan postauksen ja tarkastelemaan sen kommentteja, näkemään muidan käyttäjien profiilit ja niiden postaukset ja hakemaan sivulta sisältöä. Kirjautumaton käyttäjä ei pysty tuottamaan sivustolle sisältöä.</p>
            
            <p>Web-palvelujen määrittely ja suunnittelu -opintojaksolla tehdyssä <a target="_blank" href="http://student.labranet.jamk.fi/~K8966/projekti2/vaatimusmaarittely.pdf">vaatimusmäärittelyssä</a> on toiminnot joita lähdettiin toteuttamaan.</p>
            
        </div>
        <div class="rapsa-article">
            <h2>Käytännön toteutus</h2>
            <p>Projekti on perustettu labranetissä olevalle salt.labranet.jamk.fi virtuaalipalvelimelle, jonne on pääsy labraverkosta ssh:n kautta. Käytettyjä tekniikoita ovat Laravel, Bootstrap, MySQL, php, html, css ja javascript.</p>
            <br>
            
            <h4>Projektin rakenne</h4>
            
            <p>Laravel frameworkissa on oma rakenteensa ja tapansa nimetä tiedostoja ja kansioita. Seuraavaksi on listattu projketille oleelliset itse luodut tai muokatutut tiedostot.</p>
            
            <p>Listattujen tiedostojen<a href="http://student.labranet.jamk.fi/~K8920/salt_project"> lähdekoodit.</a></p><br>

            <p>Projekti löytyy palvelimen kansiosta:</p>
            <code>/var/www/html/laraproject</code><br><br>

            <p>Laravel:ssä funktiot kootaan controllereihin joita ohjataan reittien kautta web.php tiedostosta. Controllerit on luotu itse ja myös niiden funktiot on kirjoitettu itse. Poikkeuksena Auth kansio, joka on Laravelin oma autentikointi systeemi. Näitä tiedostoja on vain muokattu projektiin sopiviksi.</p>

            <code>
                /var/www/html/laraproject/app/Http/Controllers/<br>
                ├── Auth<br>
                │   ├── LoginController.php<br>
                │   ├── RegisterController.php<br>
                │   └── ResetPasswordController.php<br>
                ├── CommentsController.php<br>
                ├── Controller.php<br>
                ├── EditUserController.php<br>
                ├── IndexController.php<br>
                ├── PostsController.php<br>
                ├── SearchController.php<br>
                ├── UserController.php<br>
                └── ViewController.php<br><br>
            </code>

            <p>Web.php -tiedostoon on määritetty controllereilta sivuille ja toisinpäin vietävien tietojen reitit. Reitit sivustolle on määritetty itse.</p>
            <code>
                /var/www/html/laraproject/routes/<br>
                └── web.php<br>
            </code><br>

            <p>Itse sivut eli näkymät ovat blade templateja. Ne ovat muotoa <code>index.blade.php</code>. Jokaiselle sivulle oma alikansio, jossa pääsivu on <code>index.blade.php</code> ja muut sivut nimetty muuten kuvaavasti. Auth kansiossa on sisäänkirjautumis- ja rekisteröintisivut, joita on vaan muokattu. Muut sivut ja kansiot on luotu itse. Layouts kansiossa on leiska.blade.php, jossa on head ja footer tagit ja niiden sisällöt. Leiska linkatamaan muille sivuille ja se näkyy kaikkialla samanlaisena.</p>
            
            <code>
                /var/www/html/laraproject/resources/views/<br>
                ├── auth<br>
                │   ├── login.blade.php<br>
                │   └── register.blade.php<br>
                ├── layouts<br>
                │   └── leiska.blade.php<br>
                ├── nib<br>
                │   └── index.blade.php<br>
                ├── posts<br>
                │   ├── edit.blade.php<br>
                │   ├── index.blade.php<br>
                │   └── upload.blade.php<br>
                ├── profile<br>
                │   ├── edit.blade.php<br>
                │   └── index.blade.php<br>
                ├── report<br>
                │   └── index.blade.php<br>
                └── search<br>
                    └── index.blade.php<br><br>
            </code>
            
            <p>Public kansiossa on css- ja javascript-tiedostot ja kuvat. Myös storage kansio on linkattu symboolisella linkillä public kansioon.</p>
            
            <code>
                /var/www/html/laraproject/public/<br>
                ├── css<br>
                │   ├── app.css<br>
                │   ├── bootstrap.min.css<br>
                │   ├── galleria.css<br>
                │   └── thumbnail-gallery.css<br>
                ├── img<br>
                │   └── grid_noise.png<br>
                ├── js<br>
                │   ├── bootstrap.bundle.min.js<br>
                │   └── jquery.min.js<br>
                └── storage -> /var/www/html/laraproject/storage/app/public<br><br>

            </code>
            
            <p>Käyttäjiä luodessa heille luodaan storage kansioon kansio omalla nimellä. Tänne tallennetaan kaikki käyttäjän lataamat kuvat. Kuvat on nimetty käyttäjänimen ja aikaleiman mukaan, esim. <code>uploadtest_1523427598.png</code>. Avatars kansiossa on jokaisen käyttäjän profiilikuva ja ne on nimetty kättäjätaulun id:n mukaan, esim. <code>10.jpg</code>. Public kansioon on tehty symboolinen linkki storage kansiolle.</p>
            
            <code>
                /var/www/html/laraproject/public/storage/<br>
                ├── asdsad<br>
                │   └── asdsad_1523427972.jpg<br>
                ├── avatars<br>
                │   ├── 10.jpg<br>
                │   ├── 1.png<br>
                │   ├── 20.jpg<br>
                │   ├── 20.png<br>
                │   ├── 21.jpg<br>
                ├── jalter<br>
                │   ├── jalter_1523862973.jpg<br>
                │   └── jalter_1523872175.jpg<br>
                ├── Kahvikuppi<br>
                │   └── Kahvikuppi_1523867940.png<br>
                ├── Orion<br>
                │   ├── Orion_1523994166.gif<br>
                │   └── testi.png<br>
                └── uploadtest<br>
                    ├── uploadtest_1523427598.png<br>
                    └── uploadtest_1523428017.PNG<br><br>
            </code>
            
            
            
            <p>Tietokantamigraatioissa on määritelty tietokantojen taulut, kentät ja yhteydet toisiinsa. Migraatiot on tehty aiemmin suunnitellun tietokantamallin mukaan. Valmiita migraatioita olivat users ja password resets table. Users migraatiota muokattiin ja loput migraatiot on tehty itse.</p>
            
            <code>
                /var/www/html/laraproject/database/migrations/<br>
                ├── 2014_10_12_000000_create_users_table.php<br>
                ├── 2014_10_12_100000_create_password_resets_table.php<br>
                ├── 2018_03_26_170555_create_categories_table.php<br>
                ├── 2018_03_26_170759_create_tags_table.php<br>
                ├── 2018_03_26_170851_create_posts_table.php<br>
                ├── 2018_03_26_171357_create_tickets_table.php<br>
                ├── 2018_03_26_171611_create_comments_table.php<br>
                ├── 2018_03_26_171939_create_favorites_table.php<br>
                ├── 2018_03_26_172722_create_tags_on_posts_table.php<br>
                └── 2018_03_28_074515_create_follows_table.php<br><br>
            </code>
            
            <p>Tietokannan taulujen luonnin yhteydessä niille luotiin myös modelit, joissa on määritelty taulujen välisiä suhteita <code>belongsTo</code> ja <code>hasMany</code> yhteyksillä. Myös tietojen lisääminen useampi kenttä kerralla on sallittu malleihin.</p>
            
            <code>
                /var/www/html/laraproject/app/<br>
                ├── Category.php<br>
                ├── Comment.php<br>
                ├── Favorite.php<br>
                ├── Follow.php<br>
                ├── Post.php<br>
                ├── Tag.php<br>
                ├── Tags_on_Post.php<br>
                ├── Ticket.php<br>
                └── User.php<br><br>

            </code>

            
            <h4>Funktiot</h4>
            <p>Controllerit</p>
                <ul>
                    <li><code>CommentsController.php</code></li>
                        <p>kommenttien luonti ja tykkäämiset</p>

                    <li><code>EditUserController.php</code></li>
                        <p>päivitetään käyttäjän tiedot</p>
                        
                    <li><code>IndexController.php</code></li>
                        <p>sivuttaa sisällön ja järjestää etusivun postaukset uusimman tai vanhimman mukaan</p>
                        
                    <li><code>PostsController.php</code></li>
                        <p>tallentaa uudet postaukset ja poistaa ne, kommenttien poisto ja postausten päivittäminen</p>
                        
                    <li><code>SearchController.php</code></li>
                        <p>käyttäjien ja postausten haku</p>
                        
                    <li><code>UserController.php</code></li>
                        <p>haetaan käyttäjien tiedot ja käyttäjän poistaminen</p>
                       
                    <li><code>ViewController.php</code></li>
                        <p>ohjataan raportti -sivulle</p>
                       
                </ul>
        
            
            <h4>Tietokanta</h4>
            <a href="/storage/rapsa/saltdb-plan.PNG"><img class="rapsa-img" src="/storage/rapsa/saltdb-plan.PNG" alt="tietokanta" title="tietokanta"></a>
            <p class="timestamp">Tietokantamalli</p><br>

            <p>Kuvan tietokantamalli on suunniteltu Web-palvelujen määrittely ja suunnittelu -opintojaksolla ja sitä on muokattu toimivammaksi projektin edetessä. Kaikki taulut paitsi follow toimivat. Jostain syystä Laravel ei hyväksynyt kahta viittausta user taulun id kenttään. Tikettejä ja tageja ei ole otettu käyttöön toimintojen priorisoinnin vuoksi.</p>
            
            <p>Tietokannan taulut on luotu migraatioiden avulla, jotka on jo aiemmin listattu. Testidata syötettiin tauluihin <code>php artisan tinkerin</code> avulla. Myöhemmin tauluja kuitenkin jouduttiin muokkaamaan. Koska migraatioja ajaessa taulut on poistettava ja kaikki siellä ollut data menetetään, tehtiin muokkaukset suoraan sql:llä.</p><br>
            
            <h4>Testaus</h4>
            <p>Sivuston toimintoja testattiin aina, kun toimintoja saatiin valmiiksi. Yleisesti virheet saatiin korjattua tai kierrettyä. Projektia jaettiin myös kavereille ja pyydettiin testaamaan sivuston toimivuutta. Tätä kautta huomattiin, että profiilinimeksi pystyi syöttämään tyhjän kentän alt koodilla. Koska postausten nimissä käytettiin profiilinimeä, sivusto ei pystynyt näyttämään postauksia oikein tai edes luomaan kansiota käyttäjlle. Tämä estettiin rajaamalla nimen syötteeksi numerot ja kirjaimet. Sivuston toimivuutta testatiin myös mobiilissa. Aluksi hampurilaisvalikko ei toiminut virheellisten js linkitysten vuoksi. Mobiilinäkymää saatiin korjattua toimivammaksi, mutta siinä on vielä hiomista.</p>
 
        </div>
        
        <div class="rapsa-article">
            <h2>Resurssit</h2>
            <p>Ryhmä piti kirjaa projektiin käytetyistä resursseista excel taulukolla. Käytäntönä oli, että jokainen on itse vastuussa omista tunneistaan.</p><br>

            
            <table cellspacing=0 border=1>
					<tr>
						<td style=min-width:50px>PVM</td>
						<td style=min-width:50px>Mitä teki</td>
						<td style=min-width:50px>Sasu</td>
						<td style=min-width:50px>Taneli</td>
						<td style=min-width:50px>Laura</td>
						<td style=min-width:50px>Aarni</td>
					</tr>
					<tr>
						<td style=min-width:50px>6.3.</td>
						<td style=min-width:50px>Projektin aloitus</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>7.3.</td>
						<td style=min-width:50px>Virtuaalikoneen asentelu</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>13.3.</td>
						<td style=min-width:50px>Tietokanta + testidata</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>13.3.</td>
						<td style=min-width:50px>Laravel + ongelmien ratkaisu</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>13.3.</td>
						<td style=min-width:50px>Bootstrap + leiska</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>14.3.</td>
						<td style=min-width:50px>Laravel + tietokanta</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>14.3.</td>
						<td style=min-width:50px>Boorstrap</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>18.3.</td>
						<td style=min-width:50px>Tietokanta</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>2</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>19.3.</td>
						<td style=min-width:50px>Tietokanta</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>20.3.</td>
						<td style=min-width:50px>Sisäänkirjautuminen</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>20.3.</td>
						<td style=min-width:50px>Leiskat laravelliin</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>20.3.</td>
						<td style=min-width:50px>Tietokanta</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>21.3.</td>
						<td style=min-width:50px>Tietokanta</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>5</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>21.3.</td>
						<td style=min-width:50px>Boorstrap</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>21.3.</td>
						<td style=min-width:50px>Laravel, kansiot</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>26.3.</td>
						<td style=min-width:50px>Tietokanta</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1.5</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>27.3.</td>
						<td style=min-width:50px>Tietokanta</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>27.3.</td>
						<td style=min-width:50px>Kansiot käyttäjille</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>27.3.</td>
						<td style=min-width:50px>Tietokannan tiedot sivulle</td>
						<td style=min-width:50px>2</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>2</td>
					</tr>
					<tr>
						<td style=min-width:50px>27.3.</td>
						<td style=min-width:50px>Vähän kaikkea säätöä</td>
						<td style=min-width:50px>2</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>2</td>
					</tr>
					<tr>
						<td style=min-width:50px>28.3.</td>
						<td style=min-width:50px>Tietokanta säätöö</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>28.3.</td>
						<td style=min-width:50px>Upload</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>28.3.</td>
						<td style=min-width:50px>Käyttäjät/sisäänkirjautuminen</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>3.4.</td>
						<td style=min-width:50px>Edit profile</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>3.4.</td>
						<td style=min-width:50px>Profiili sivu</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>3.4.</td>
						<td style=min-width:50px>Upload/posts</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>4.4.</td>
						<td style=min-width:50px>Edit profile</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>4.4.</td>
						<td style=min-width:50px>Profiili sivu / tietokanta</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>4.4.</td>
						<td style=min-width:50px>Etusivu / tietojen listaus</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>4.4.</td>
						<td style=min-width:50px>Upload/posts</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>8.4.</td>
						<td style=min-width:50px>Käyttäjän postaukset</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>9.4.</td>
						<td style=min-width:50px>Käyttäjän postaukset</td>
						<td style=min-width:50px>2.5</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>9.4.</td>
						<td style=min-width:50px>Etusivu / Register Controller</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>10.4.</td>
						<td style=min-width:50px>Upload/posts</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>10.4.</td>
						<td style=min-width:50px>Profiili</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>10.4.</td>
						<td style=min-width:50px>Edit profile, sisäänkirjautuminen</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>10.4.</td>
						<td style=min-width:50px>Etusivu</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
					</tr>
					<tr>
						<td style=min-width:50px>11.4.</td>
						<td style=min-width:50px>Upload/posts</td>
						<td style=min-width:50px>4</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>11.4.</td>
						<td style=min-width:50px>Profiili</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>11.4.</td>
						<td style=min-width:50px>Etusivu, leiska css</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>4</td>
					</tr>
					<tr>
						<td style=min-width:50px>11.4.</td>
						<td style=min-width:50px>Profiilin muokkaaminen</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>4</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>15.4.</td>
						<td style=min-width:50px>Profiili kuva</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>16.4.</td>
						<td style=min-width:50px>Profiili, post</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3.5</td>
						<td style=min-width:50px>5</td>
					</tr>
					<tr>
						<td style=min-width:50px>16.4.</td>
						<td style=min-width:50px>Upload</td>
						<td style=min-width:50px>3.5</td>
						<td style=min-width:50px>5.5</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>13.4.</td>
						<td style=min-width:50px>Upload</td>
						<td style=min-width:50px>1.5</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>15.4.</td>
						<td style=min-width:50px>Logo</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1</td>
					</tr>
					<tr>
						<td style=min-width:50px>17.4.</td>
						<td style=min-width:50px>Haku</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>4</td>
						<td style=min-width:50px>4</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>17.4.</td>
						<td style=min-width:50px>Css/kommentit</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>4</td>
					</tr>
					<tr>
						<td style=min-width:50px>17.4.</td>
						<td style=min-width:50px>Kommentit</td>
						<td style=min-width:50px>4</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>18.4.</td>
						<td style=min-width:50px>Css/ulkoasu + kommenttien näyttö</td>
						<td style=min-width:50px>4.5</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>4.5</td>
					</tr>
					<tr>
						<td style=min-width:50px>18.4.</td>
						<td style=min-width:50px>Sorttaus/postauksen päivittäminen/kommentin poisto</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>4.5</td>
						<td style=min-width:50px>4.5</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>18.4.</td>
						<td style=min-width:50px>hover-efektin kanssa taistelu</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>2</td>
					</tr>
					<tr>
						<td style=min-width:50px>19.4.</td>
						<td style=min-width:50px>Tykkääminen</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>5.5</td>
						<td style=min-width:50px>5.5</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>19.4.</td>
						<td style=min-width:50px>Ulkoasu</td>
						<td style=min-width:50px>5.5</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>5.5</td>
					</tr>
					<tr>
						<td style=min-width:50px>19.4.</td>
						<td style=min-width:50px>Comment/Like -systeemin korjausta/hajotusta</td>
						<td style=min-width:50px>1.5</td>
						<td style=min-width:50px>1.5</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>19.4.</td>
						<td style=min-width:50px>postisivun ulkoasun korjaus, adminille oma poistonappi</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1</td>
					</tr>
					<tr>
						<td style=min-width:50px>21.4.</td>
						<td style=min-width:50px>Profiilin poisto, kommenttien poiston sotkemista, rapsaa</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>4</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>21.4.</td>
						<td style=min-width:50px>Upload/index</td>
						<td style=min-width:50px>1</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>22.4.</td>
						<td style=min-width:50px>Logoa</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1</td>
					</tr>
					<tr>
						<td style=min-width:50px>22.4.</td>
						<td style=min-width:50px>Search</td>
						<td style=min-width:50px>1</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>22.4.</td>
						<td style=min-width:50px>Kommenttien poistoa</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>23.4.</td>
						<td style=min-width:50px>Raportti/kommenttien poisto</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>4.5</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>23.4.</td>
						<td style=min-width:50px>Css/Logo</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>4</td>
					</tr>
					<tr>
						<td style=min-width:50px>23.4.</td>
						<td style=min-width:50px>css/kommenttien poisto/postit</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>23.4.</td>
						<td style=min-width:50px>kommenttien poisto/postit</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>23.4.</td>
						<td style=min-width:50px>Koodin kommentointi</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>24.4.</td>
						<td style=min-width:50px>Raportti</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>24.4.</td>
						<td style=min-width:50px>Loppu viilausta, unlike</td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px>24.4.</td>
						<td style=min-width:50px>Loppu viilausta, unlike</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>3</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
                    <tr>
						<td style=min-width:50px>24.4.</td>
						<td style=min-width:50px>Mobiilinäkymän parantelua, hamppari toimimaan</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>2</td>
					</tr>
                    <tr>
						<td style=min-width:50px>24.4.</td>
						<td style=min-width:50px>alasivusysteemi etusivulle</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>1</td>
					</tr>
                <!--
					<tr>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
					<tr>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
						<td style=min-width:50px></td>
					</tr>
-->
					<tr>
						<td style=min-width:50px>yht.</td>
						<td style=min-width:50px></td>
						<td style=min-width:50px>69</td>
						<td style=min-width:50px>66</td>
						<td style=min-width:50px>73.5</td>
						<td style=min-width:50px>69</td>
					</tr>
				</table>
            

        </div>
        
        <div class="rapsa-article">
            <h2>Itsearvio</h2>

            
            <p>Halusimme valita projektin tekemiseen Laravel frameworkin, koska siitä voisi olla hyötyä jatkossa ja projektia tehdessä voisi opetella uutta. Aikaisempaa kokemusta Laravelista ei juurikaan ollut ja tämä tuotti varsinkin alussa vaikeuksia. Laraveliin pääsi kuitenkin sisälle googlettelun ja ahkeran kyselemisen jälkeen. Virtuaalipalvelimen valitseminen projektin tekoon oli myös hyvä valinta, koska kaikki tiedostot olivat samassa paikassa. Välillä tästä koitui kuitenkin ongelmia, jos samaa tiedostoa muokattiin yhtäaikaa. Alussa oli myös tietokantamigraatioiden kanssa ongelmia, mutta niistä päästiin yli. Myös formit tuottivat harmia, koska monta formia ei oikein toiminut samalla sivulla. Tämä ratkaistiin muuttamalla yksi formi linkiksi. Työntekoa helpotti kuitenkin jo syksyllä tehty suunnitelma projektista.</p>
            
            <p>Alussa ryhmän keskittyminen oli hieman hakusessa, mutta muiden kurssien tippuessa ja koodin toimiessa motivaatiokin nousi ja tekeminen lisääntyi. Projektin tekeminen oli mielekästä ja mukavaa. Kaikille jakautui oma vastuualue: Taneli hoiti koodaamista, Laura tietokantaa, Aarni ulkoasua ja Sasu koodaamista ja ulkoasua, mutta kaikki tekivät myös kaikkea.</p>
            
            <p>Olemme tyytyväisiä projektin lopputulokseen, koska se on ehjä ja toimiva kokonaisuus. Alkuperäinen suunnitelma oli myös melko laaja ja alussa priorisoimme toimintoja joita lähdettiin tekemään. Joistain toiminnoista jouduttiin luopumaan ajanpuutteen ja osa toimimattomuuden takia. Kuitenkin tärkeimmät toiminnot on saatu toimimaan ja myös ulkoasuun on pystytty panostamaan.  Projekti on opettanut paljon Laravelista ja projektityöskentelystä.</p>
            
            <br>
            <p>Toimii</p>
            <ul>
                <li>Profiilin luominen, muokkaaminen ja poistaminen</li>
                <li>Postausten julkaisu, muokkaaminen js poistaminen</li>
                <li>Kommentointi ja kommentin poisto</li>
                <li>Tykkääminen ja tykkäämisen poisto</li>
                <li>Käyttäjien ja postausten haku</li>
                <li>Postausten järjestäminen uusimmasta vanhimpaan</li>
                <li>Postauslistan jako alasivuihin</li>
            </ul>
            
            <p>Jatkokehitettävää</p>
            <ul>
                <li>Tagit</li>
                <li>Tiketit, admin toiminnot</li>
                <li>Postausten järjestäminen useamman ehdon mukaan</li>
                <li>Käyttäjien seuraaminen</li>
                <li>Kuvien ja kansioiden poistaminen palvelimelta, kun ne poistetaan tietokannasta</li>
                <li>Yleistä datan tarkistusta syötettäessä tietokantaan/formeille</li>
                <li>Sisällön reaaliaikainen esille tuonti</li>
                <li>Mobiilinäkymien hiominen</li>
            </ul>
    
            <p>Arvosanaehdotukset ja kommentit:</p>
            <ul>
                <li>Sasu Karvonen: 4</li>
                <p>Projekti oli kiinnostava. Alussa oli hieman hankaluuksia laravelin kanssa. Kaikki piti melkein opetella nollasta, mutta virheiden kautta voittoon. Olen hyvin iloinen, että päätimme lainata virtuaalipalvelimen sillä se helpotti hyvin paljon. Projektissa ohjelmoin controllereita ja säädin tyylimäärittelyjä pääasiassa, mutta tein oikeastaan vähän kaikkea. Olen tyytyväinen projektiin  vaikka muutama suunniteltu funktio puuttuukin.</p>
                <li>Laura Pekkanen: 4</li>
                <p>Projektin tekeminen tuntui aluksi vaikealta ja haastavalta Laravelin takia. Itse vastasin tietokannasta ja alussa sen toimimaan saaminen oli vaikeaa (koska Laravel). Homma alkoi kuitenkin sujua ja projektia oli mukava tehdä. Olen tyytyväinen ryhmän työskentelyyn ja projektin lopputulokseen. Projektia tehdessä oppi paljon Laravelista ja koodaamisesta yleensä.</p>
                <li>Taneli Sormunen: 4</li>
                <p>Projekti oli mielenkiintoinen ja haastava. Laravel oli aluksi aika hankala ymmärtää ja sisäistää, mutta tuskailujen jälkeen asiat helpottuivat huomattavasti ja Laravelin hyvät puolet alkoivat tulla esiin paremmin.
                Panokseni projektissa oli lähinnä ohjelmoinnin parissa.
                Lopputulokseen olen henkilökohtaisesti tyytyväinen, lopputulos on mielestäni toimiva ja näyttävä, vaikka joitain suunniteltuja ominaisuuksia vielä puuttuukin.</p>
                <li>Aarni Ylhäinen: 4</li>
                <p>Projekti toi mukavasti haastetta, erityisesti kun php oli suhteellisen uutta ja php frameworkit vielä uudempaa. Alkuviikkoina tuli tuskailtua laravelin kanssa mutta projektin kunnolla startattua laravel alkoikin käydä kunnolla tutuksi. Tein projektissa leiskaa sekä jonkin verran ohjelmointia, ja olen tyytyväinen lopputulokseen. Suunniteltuja ominaisuuksia puuttuu muutamia (tagisysteemi, admintoiminnot yms.) mutta kaikki toimii suoraan sanottuna yllättävän hyvin.</p>
            </ul>
        </div> 
    </div>
    <!-- /.container -->
@endsection

