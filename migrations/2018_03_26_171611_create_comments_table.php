
<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->string('commenttext', 1000);
            $table->integer('user_id')->unsigned();
	        $table->foreign('user_id')
              ->references('id')->on('users')
              ->onDelete('no action')
              ->onUpdate('cascade');
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')
              ->references('id')->on('posts')
              ->onDelete('cascade')
              ->onUpdate('cascade');
            $table->timestamps();
            $table->timestamp('published_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
