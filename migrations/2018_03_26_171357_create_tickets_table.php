<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id')->unsigned();
	    $table->string('reason', 50);
            $table->string('description', 1000);
            $table->integer('post_id')->unsigned();
            $table->foreign('post_id')
              ->references('id')->on('posts')
              ->onDelete('cascade')
              ->onUpdate('cascade');
            $table->integer('user_id')->unsigned();
	        $table->foreign('user_id')
              ->references('id')->on('users')
              ->onDelete('no action')
              ->onUpdate('cascade');
            $table->timestamps();
            $table->timestamp('published_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
