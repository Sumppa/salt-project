<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Storage;

class EditUserController extends Controller
{
    
    public function edit(User $user)
    {
        $user = Auth::user();
        return view ('profile.edit')->with('user', $user);
    }
    
    // Update edited user information
    public function update(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|max:100,',
            'password' => 'required|string|min:6',
            'description' => 'string|max:1000',
        ]);
        
        // Check if user want's to upload new profile image
        if($request->hasFile('img')){
            $name = Auth::user()->id;
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            //get filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get file extension (.jpg, .png, etc)
            $extension = $request->file('img')->getClientOriginalExtension();
            //Filename to storage
            $fileNameToStore = $name.'.'.$extension;
            //Upload file
            $path = $request->file('img')->storeAs('public/avatars', $fileNameToStore);
        } else {
            
        // If not, keep old image
            $fileNameToStore = Auth::user()->img;
        }
        
        // Save changes
        $user_id = $request->input('id');
        $user = User::find($user_id);
        $user->email = $request->input('email');
        $user->description = $request->input('description');
        $user->img = $fileNameToStore;
        $user->password = bcrypt($request->input('password'));
        $user->save();
        return redirect ('profile/'.Auth::user()->id);
    }
}