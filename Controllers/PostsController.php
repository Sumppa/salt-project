<?php

namespace App\Http\Controllers;

use App\User;
use App\Comment;
use App\Post;
use App\Favorite;
use App\Http\Controllers\Validator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
//use Request;

class PostsController extends Controller
{
    // Get post information
    public function show($id) {
        $posti = Post::find($id);
        $posts = Post::findOrFail($id);
        $comments = Comment::where('post_id', $id)->get();
        $count = Favorite::where('post_id', $id)->count();
        return view('posts.index')->with('posts', $posts)->with('comments', $comments)->with('posti', $posti)->with('count', $count);
    }
    // Return upload view with user id
    public function create() {
        $id = Auth::user()->id;
        return view('posts.upload')->with('id', $id);
    }
    // Save uploaded image into database
    public function store(Request $request) {
        $this->validate($request, [
                        'title' => 'required',
                        'user_id' => 'required',
                        'category_id' => 'required',
                        'img' => 'image|max:1000000'
        ]);
            
        //$name = Auth::user()->name;
        if($request->hasFile('img')){
            $name = Auth::user()->name;
            $filenameWithExt = $request->file('img')->getClientOriginalName();
            //get filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            //get ext
            $extension = $request->file('img')->getClientOriginalExtension();
            //Filename to storage
            $fileNameToStore = $name.'_'.time().'.'.$extension;
            //Upload file
            $path = $request->file('img')->storeAs('public/'.$name, $fileNameToStore);
            
        }
        
        // Create new post
        $post = new Post;
        $post->title = $request->input('title');
        $post->user_id = $request->input('user_id');
        $post->description = $request->input('description');
        $post->category_id = $request->input('category_id');
        $post->img = $fileNameToStore;
        $post->save();
        return redirect('index');

    }
    
    // Delete post
    public function delete($id) {
        $post = Post::find($id);
        $post->delete($id);   
        return redirect('index');
    }
    
    // Delete comment on post
    public function deleteComment($id) {
        $comment = Comment::find($id);
        $comment->delete($id);
        return redirect('/posts/'.$comment->post_id);   
    }
    
    // Get post id to edit view
    public function edit($id)
    {
        $post = Post::find($id);
        return view ('posts.edit')->with('post', $post);
    }
    
    // Update edited post information
    public function update_edit(Request $request) {
        
        $this->validate($request, [
            'title' => 'required|string|max:50,',
            'description' => 'string|max:1000',
        ]);
        
        $post_id = $request->input('id');
        $post = Post::find($post_id);
        $post->title = $request->input('title');
        $post->description = $request->input('description');
        $post->save();
        return redirect ('/posts/'.$post_id);
    }     
}











