<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Html\HtmlFacade;
use App\Http\Controllers\Controller;


class IndexController extends Controller
{
    // Sort index page images into array
    public function index($sort_act) {
        
        if ($sort_act == 'newest')
        {
            $posts = Post::latest()->paginate(20);
        } 
        elseif ($sort_act == 'oldest') {
            $posts = Post::paginate(20);
        }
        
        
        return view('home.index', ['posts' => $posts]);
        
        /*
        $posts = Post::All();
        if ($sort_act == 'newest')
        {
            $posts = $posts->reverse();
        } elseif ($sort_act == 'oldest') {
            $posts = $posts;
        }

        //return $posts;
        return view('home.index')->with('posts', $posts);*/
    }

}