<?php
namespace App\Http\Controllers;

use App\User;
use App\Post;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Request;

class UserController extends Controller
{  
    // Get user informations and user's posts
    // Sort posts in reverse order
    public function show($id)
    {
        $users = User::findOrFail($id);
        //return view('profile.index')->with('users', $users);
        //return $users;
        
        $posts = Post::where('user_id',$id)->get();
        //uusin ensin
        $posts = $posts->reverse();
        return view('profile.index')->with('users', $users)->with('posts', $posts)->with('id', $id);
    }
    // Delete user
    public function delete_profile($id) {
        $user = User::find($id);
        $user->delete($id);   
        return redirect('index');
    }
    
}
