<?php

namespace App\Http\Controllers;

use App\Post;
use App\User;
use App\Http\Controllers\Validator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
//use Request;

class SearchController extends Controller
{
    // Search users by name and posts by title
    public function search(Request $request) {
        $query = $request->input('search');
        if ($query == '') {
          $posts = array();
          $users = array();  
        } else {
        $posts = Post::where('title', 'like', '%'.$query.'%')->get();
        $users = User::where('name', 'like', '%'.$query.'%')->get();
            }
        return view ('search.index')->with('query', $query)->with('posts', $posts)->with('users', $users);
    }

}
