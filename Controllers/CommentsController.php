<?php

namespace App\Http\Controllers;

use App\User;
use App\Comment;
use App\Post;
use App\Favorite;
use App\Http\Controllers\Validator;
use Illuminate\Http\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    
    // This function handles likes on posts
    // When user press "like" button, here we check if the user already likes the post or not
    public function like($posti) {
      $id_user = Auth::user()->id;
        
      //$page = $request->input('post_id');
      if(Favorite::where('user_id', $id_user)->where('post_id', $posti)->exists()) {
          //$alert = "<script>alert('You already like this post.')</script>";
          $favorite = Favorite::where('user_id', $id_user)->where('post_id', $posti);
          $favorite->delete();   
          return redirect('/posts/'.$posti);
      } else {
          $like = new Favorite;
          $like->user_id = $id_user;
          $like->post_id = $posti;
          $like->save();
          return redirect ('/posts/'.$posti);
      }
    }
    
    // Creating new comment on posts
    public function saveComment(Request $request) {
        $this->validate($request, [
                'commenttext' => 'required'
            ]);
        $redirect = $request->input('post_id');
        $id_user = Auth::user()->id;
        $comment = new Comment;
        $comment->post_id = $request->input('post_id');
        $comment->commenttext = $request->input('commenttext');
        $comment->user_id = $id_user;
        $comment->save();
        return redirect('posts/'.$redirect);
        
    }
}
